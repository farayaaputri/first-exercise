from django.shortcuts import render

# Create your views here.
def index(request):
    response = {'name': 'Faraya', 'author' : 'Faraya Agatha Putri'}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
